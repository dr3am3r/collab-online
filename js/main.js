'use strict';

const DB_URL = 'https://dr3am3r.cloudant.com/collaboration';
const REQUEST_OPTIONS = { mode: 'cors', headers: { Authorization: 'Basic ZHIzYW0zcjpzaG90Z3Vu' } };

// doc id 
let docId = '26797a46d44cce0d22ecf2f29545ad0a';
let currentDocument = null;
let lastSeq = null;

class Couch {

    getById(id) {   //DB_URL + '/' + id
        return fetch(`${DB_URL}/${id}`, REQUEST_OPTIONS)
            .then(response => response.json());
    }

    save(doc) { // { _id: ..., _rev: ...., }
        let id = docId;
        delete doc._id;
        return fetch(`${DB_URL}/${id}`,
            Object.assign({}, REQUEST_OPTIONS, { method: 'PUT', body: JSON.stringify(doc) })
        ).then(response => response.json());
    }

    pollChanges(id) {
        let url = `${DB_URL}/_changes?include_docs=true`;
        if (lastSeq) {
            url += '&since=' + lastSeq;
        }
        return fetch(url, {
            method: 'GET',
            mode: 'cors',
            headers: {
                Authorization: 'Basic ZHIzYW0zcjpzaG90Z3Vu',
                'Content-Type': 'application/json',
                Accept: 'application/json'
            }
        }).then(response => response.json());
    }
}

const couch = new Couch();

$(document).ready(function() {

    couch.getById(docId).then(doc => {
        currentDocument = doc;
        $('#details').text(doc.content);
    });
    setInterval(() => {
        couch.pollChanges(docId).then(data => {
            console.log('got changes', data);
            lastSeq = data.last_seq;
            
            if (data.results.length) {
                let updatedDoc = data.results[0].doc;
                console.log('updated Doc: ', updatedDoc);
                currentDocument.content = updatedDoc.content;
                currentDocument._rev = updatedDoc._rev;
                $('#details').text(currentDocument.content);
            }
        });
    }, 1000);
});

$('#btnSave').click(evt => {
    evt.preventDefault();
    evt.stopPropagation();
    currentDocument.content = $('#details').val();
    console.log(currentDocument);
    couch.save(currentDocument).then(data => {
        alert('Saved!');
        console.log(data);
        currentDocument._rev = data.rev;
    })
});


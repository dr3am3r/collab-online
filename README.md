###Real-time collaboration



###TODO

* create html template with big textarea field. it will contain the document contents'
* create a CRUD adapter to Couchdb, use https://dr3am3r:shotgun@dr3am3r.cloudant.com/collaboration
* to get document by ID, i.e. for document '26797a46d44cce0d22ecf2f29545ad0a' the request will be
* GET https://dr3am3r.cloudant.com/collaboration/26797a46d44cce0d22ecf2f29545ad0a - view
* PUT https://dr3am3r.cloudant.com/collaboration/26797a46d44cce0d22ecf2f29545ad0a - update
* POST https://dr3am3r.cloudant.com/collaboration - create
* Subscribe for db changes: GET https://dr3am3r.cloudant.com/collaboration/_changes


###Using fetch
fetch('https://dr3am3r.cloudant.com/collaboration/26797a46d44cce0d22ecf2f29545ad0a', { mode: 'cors', headers: { Authorization: 'Basic ZHIzYW0zcjpzaG90Z3Vu' } }).then(response => response.json()).then(data => console.log(data))